#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netinet/udp.h>
#include <arpa/inet.h>
#include <linux/if_ether.h>
#include <net/if.h>
#include <linux/ip.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <pthread.h>

/*default snap (maximo bytes por paquete capturado)*/
#define SNAP_LEN 1518
#define SIZE_ETHERNET 14
#define ETHER_ADDR_LEN 6

/*Cabecera Ethernet*/
struct sniff_ethernet{
    u_char ether_dhost[ETHER_ADDR_LEN];
    u_char ether_shost[ETHER_ADDR_LEN];
    u_short ether_type;
};

/*Cabecera IP*/
struct sniff_ip{
    u_char ip_v;
    u_char ip_tos;
    u_short ip_len;
    u_short ip_id;
    u_short ip_off;
    #define IP_RF 0x8000
    #define IP_DF 0x4000
    #define IP_MF 0x2000
    #define IP_OFFMASK 0x1fff
    u_char ip_ttl;
    u_char ip_p;
    u_short ip_sum;
    struct in_addr ip_src, ip_dst;
};
#define IP_HL(ip) (((ip)->ip_v) & 0x0f)
#define IP_V(ip)  (((ip)->ip_v) >> 4)

/*Cabecera TCP*/
typedef u_int tcp_seq;
struct sniff_tcp{
    u_short th_sport;
    u_short th_dport;
    tcp_seq th_seq;
    tcp_seq th_ack;
    u_char th_offx2;
    #define TH_OFF(th) (((th)->th_offx2 & 0xf0) >> 4)
    u_char th_flags;
    #define TH_FIN 0x01
    #define TH_SYN 0x02
    #define TH_RST 0x04
    #define TH_PUSH 0x08
    #define TH_ACK 0x10
    #define TH_URG 0x20
    #define TH_ECE 0x40
    #define TH_CWR 0x80
    #define TH_FLAGS (TH_FIN | TH_SYN | TH_RST | TH_PUSH | TH_ACK | TH_URG | TH_ECE | TH_CWR)
    u_short th_win;
    u_short th_sum;
    u_short th_urp;
};

FILE *logfile;

struct Datos{
    char* tarjeta_red;
    int cantidad_paquetes;
    int sock_raw;
};

struct Trama{
    int id;
    int length;
    int difusion;
    unsigned char* buffer;
    struct sniff_ethernet *ethernet;
    struct sniff_ip *ip;
    struct Trama *siguiente;
};

struct CabeceraIP{
    unsigned int versionIp;
    unsigned int largo;
    unsigned int tipoDeServicios;
    unsigned int largoTotal;
    unsigned int identificacion;
    unsigned int desplazamientoDeFragmento;
    unsigned int ttl;
    unsigned int protocolo;
    unsigned int VerificacionDeSuma;
    unsigned int fuente;
    unsigned int destino;
};

struct Trama *primero, *ultimo, *aux;
pthread_t hilo_capturador, hilo_analizador;

void *capturador(void *datos_void_ptr);
void *analizador(void *datos_void_ptr);
int Difusion(unsigned char c);

int main(int argc, char** argv){
    struct Datos datos;

    if(argc > 2){
        datos.tarjeta_red = argv[1];
        datos.cantidad_paquetes = atoi(argv[2]);
        logfile = fopen("log.txt", "w");

        if(logfile == NULL){
            fprintf(stderr, "Error creando logfile %d\n", datos.sock_raw);
            return -1;
        }

        struct ifreq ethreq;

        printf("Tarjeta de red: %s\n", datos.tarjeta_red);
        fprintf(logfile, "\n");
        fprintf(logfile, "Tarjeta de red: %s\n", datos.tarjeta_red);
        printf("Cantidad de paquetes a capturar: %d\n", datos.cantidad_paquetes);
        fprintf(logfile, "Cantidad de paquetes a capturar: %d\n", datos.cantidad_paquetes);

        datos.sock_raw = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
        if(datos.sock_raw < 0){
            fprintf(stderr, "Error creando socket %d\n", datos.sock_raw);
            return -1;
        }
        printf("Creación de socket: OK.\n");

        strncpy(ethreq.ifr_name, datos.tarjeta_red, IFNAMSIZ);

        int x = ioctl(datos.sock_raw, SIOCGIFFLAGS, &ethreq);
        if(x < 0){
            fprintf(stderr, "Error obteniendo IFFLAGS %d\n", x);
            return -1;
        }
        printf("Obtención de IFFLAGS: OK.\n");
        ethreq.ifr_flags |= IFF_PROMISC;
        
        x = ioctl(datos.sock_raw, SIOCSIFFLAGS, &ethreq);
        if(x < 0){
            fprintf(stderr, "Error seteando IFFLAGS %d\n", x);
            return -1;
        }
        printf("Reemplazo de IFFLAGS: OK.\n");

        if(pthread_create(&hilo_capturador, NULL, capturador, &datos)) {
            fprintf(stderr, "Error creando hilo_capturador\n");
            return 1;
        }

        if(pthread_join(hilo_capturador, NULL)) {
            fprintf(stderr, "Error uniendo hilo_capturador\n");
            return 2;
        }
    }else{
        fprintf(stderr, "Error al ejecutar el Sniffer, faltan argumentos.\n");
        return -1;
    }

    return 0;
}

void *capturador(void *datos_void_ptr){
    struct Datos *datos = (struct Datos *) datos_void_ptr;
    char* buffer[datos->cantidad_paquetes];
    
    struct sockaddr saddr;
    struct Trama *nuevo;
    int saddr_len = sizeof(saddr);
    int buflen, size_ip, i = 0;

    fprintf(stderr, "Captura de paquetes: Capturando...\n");
    while(i < datos->cantidad_paquetes){
        buffer[i] = (unsigned char *) malloc(65536);
        memset(buffer[i], 0, 65536);
        struct sniff_ethernet *ethernet;
        struct sniff_ip *ip;
        
        buflen = recvfrom(datos->sock_raw, buffer[i], 65536, 0, &saddr, (socklen_t *) &saddr_len);

        if(buflen < 0){
            fprintf(stderr, "Error capturando paquetes (recvfrom)\n");
            return NULL;
        }

        ethernet = (struct sniff_ethernet*)(buffer[i]);
        
        if(ntohs(ethernet->ether_type) == 0x0800){
            ip = (struct sniff_ip*)(buffer[i] + SIZE_ETHERNET);
            size_ip = IP_HL(ip)*4;
            if(size_ip < 20){
                fprintf(stderr, "Largo de cabecera IP invalida: %u bytes\n", size_ip);
                return NULL;
            }
            
            nuevo = (struct Trama*) malloc(sizeof(struct Trama));
            
            nuevo->id = i;
            nuevo->length = buflen;
            //nuevo->difusion = Difusion(buffer[0][0]);
            nuevo->ethernet = ethernet;
            nuevo->ip = ip;
            nuevo->buffer = buffer[i];

            nuevo->siguiente = NULL;
            if(primero == NULL){
                primero = nuevo;
                ultimo = nuevo;
            }else{
                ultimo->siguiente = nuevo;
                ultimo = nuevo;
            }
            
        }
        
        i++;
    }
   fprintf(stderr,"Captura de paquetes: OK.\n");

    if(pthread_create(&hilo_analizador, NULL, analizador, &datos)) {
        fprintf(stderr, "Error creando hilo_analizador\n");
        return NULL;
    }

    if(pthread_join(hilo_analizador, NULL)) {
        fprintf(stderr, "Error uniendo hilo_analizador\n");
        return NULL;
    }
    return NULL;
}

void *analizador(void *datos_void_ptr){
    struct Datos *datos = (struct Datos *) datos_void_ptr;
    int i = 0, eth = 0, wifi = 0, ipv4 = 0, ipv6 = 0, arp = 0, cfe = 0, sm = 0;
    aux = primero;
    char *payload;
    struct sniff_tcp *tcp;
    int size_ip;
    int size_tcp;
    int size_payload;

    printf("\n"); fprintf(logfile, "\n");
    
    while(aux!=NULL){
        size_tcp = 0;
        printf("-------------------- Paquete %d --------------------", aux->id);
        fprintf(logfile, "-------------------- Paquete %d --------------------", aux->id);
        
        printf("\nDirección ip fuente: %s", inet_ntoa(aux->ip->ip_src));
        fprintf(logfile, "\nDirección ip fuente: %s", inet_ntoa(aux->ip->ip_src));
        printf("\nDirección ip destino: %s", inet_ntoa(aux->ip->ip_dst));
        fprintf(logfile, "\nDirección ip destino: %s", inet_ntoa(aux->ip->ip_dst));
        printf("\nLongitud de cabecera: %d", IP_HL(aux->ip)*4);
        fprintf(logfile, "\nLongitud de cabecera: %d", IP_HL(aux->ip)*4);
        printf("\nLongitud total: %d", ntohs(aux->ip->ip_len));
        fprintf(logfile, "\nLongitud total: %d", ntohs(aux->ip->ip_len));
        printf("\nIdentificador: %d", ntohs(aux->ip->ip_id));
        fprintf(logfile, "\nIdentificador: %d", ntohs(aux->ip->ip_id));
        printf("\nTiempo de vida: %d", (unsigned int) aux->ip->ip_ttl);
        fprintf(logfile, "\nTiempo de vida: %d", (unsigned int) aux->ip->ip_ttl);
        printf("\nProcolo capa superior: ");
        fprintf(logfile, "\nProcolo capa superior: ");
        switch(aux->ip->ip_p){
            case 0x01: printf("ICMPv4"); fprintf(logfile, "ICMPv4"); break;
            case 0x02: printf("IGMP"); fprintf(logfile, "IGMP"); break;
            case 0x04: printf("IP"); fprintf(logfile, "IP"); break;
            case 0x06: printf("TCP"); fprintf(logfile, "TCP"); break;
            case 0x11: printf("UDP"); fprintf(logfile, "UDP"); break;
            case 0x29: printf("IPv6"); fprintf(logfile, "IPv6"); break;
            case 0x59: printf("OSPF"); fprintf(logfile, "OSPF"); break;
            default: printf("No reconocido"); fprintf(logfile, "No reconocido");
        }
        printf("\n");
        fprintf(logfile, "\n");
        size_ip = IP_HL(aux->ip)*4;
        if(aux->ip->ip_p == 0x06){
            tcp = (struct sniff_tcp*)(aux->buffer + SIZE_ETHERNET + size_ip);
            size_tcp = TH_OFF(tcp)*4;
            if(size_tcp < 20){
                fprintf(stderr, "Largo de cabecera TCP invalida: %u bytes\n", size_tcp);
                return NULL;
            }
            payload = (u_char *)(aux->buffer + SIZE_ETHERNET + size_ip + size_tcp);
            size_payload = ntohs(aux->ip->ip_len)- (size_ip + size_tcp);

            printf("\nLongitud de carga útil: %d", size_payload);
            fprintf(logfile, "\nLongitud de carga útil: %d", size_payload);
        }
        
        printf("\nTipo de Servicio: %02x", ntohs(aux->ip->ip_tos));
        fprintf(logfile, "\nTipo de Servicio: %02x", ntohs(aux->ip->ip_tos));

        //Datagrama fragmentado o no
        printf("\nFragmentación: ");
        fprintf(logfile, "\nFragmentación: ");
        if(aux->ip->ip_off == IP_MF){
            printf("Esta fragmentado");
            fprintf(logfile, "Esta fragmentado");
        }else{
            printf("No esta fragmentado");
            fprintf(logfile, "No esta fragmentado");
        }
        //Si esta fragmentado dentificar si es el primer, intermedio o ultimo datagrama
        //printf("\nDesplazamiento de fragmentación: %02x", aux->ip->ip_off);
        //fprintf(logfile, "\nDesplazamiento de fragmentación: %02x", aux->ip->ip_off);
        printf("\nDesplazamiento de fragmentación: %d", aux->ip->ip_off);
        fprintf(logfile, "\nDesplazamiento de fragmentación: %d", aux->ip->ip_off);
        //Indicar primer y ultimo byte que contiene el datagrama
        printf("\nPrimer y último byte del datagrama: %02x %02x", aux->buffer[0], aux->buffer[aux->length]);
        fprintf(logfile, "\nPrimer y último byte del datagrama: %02x %02x", aux->buffer[0], aux->buffer[ntohs(aux->ip->ip_len)+(size_ip + size_tcp)]);
        //Datagrama con opciones o sin opciones
        printf("\nOpciones: ");
        fprintf(logfile, "\nOpciones: ");
        if(size_ip <= 20){
            printf("Sin opciones");
            fprintf(logfile, "Sin opciones");
        }else{
            printf("Con opciones");
            fprintf(logfile, "Con opciones");
        }
        //Si tiene opciones indicar cuales son
        /*if(ntohs(aux->cabIP->desplazamientoDeFragmento) > 0)
        printf("\n%d", ntohs(aux->cabIP->desplazamientoDeFragmento));*/
        

        printf("\n"); fprintf(logfile, "\n");
    
        aux = aux->siguiente;
        i++;
    }
    if(i == 0){
        printf("Lista vacia.\n");
    }
    /*printf("\n Resumen de tramas:\n"); fprintf(logfile, "\n Resumen de tramas:\n");
    printf("Tramas capturadas: %d\n", i);
    fprintf(logfile, "Tramas capturadas: %d\n", i);
    printf("Tramas Ethernet II (Analizadas): %d\n", eth);
    fprintf(logfile, "Tramas Ethernet II (Analizadas): %d\n", eth);
    printf("Tramas IEEE 802.3 (No analizadas): %d\n", wifi);
    fprintf(logfile, "Tramas IEEE 802.3 (No analizadas): %d\n", wifi);

    printf("\n Resumen protocolos capa superior:\n"); fprintf(logfile, "\n Resumen protocolos capa superior:\n");

    printf("IPv4: %d\n", ipv4);
    fprintf(logfile, "IPv4: %d\n", ipv4);
    printf("IPv6: %d\n", ipv6);
    fprintf(logfile, "IPv6: %d\n", ipv6);
    printf("ARP: %d\n", arp);
    fprintf(logfile, "ARP: %d\n", arp);
    printf("Control de flujo Ethernet: %d\n", cfe);
    fprintf(logfile, "Control de flujo Ethernet: %d\n", cfe);
    printf("Seguridad MAC: %d\n", sm);
    fprintf(logfile, "Seguridad MAC: %d\n", sm);*/

    printf("\n"); fprintf(logfile, "\n");
    return NULL;
}

int Difusion(unsigned char c){
    int binario[7];
    int j=0;
    for(int i = 7; i>=0; --i){
        //printf("%d ", (c >> i) & 1? 1:0);
        binario[j] = (c >> i) & 1? 1:0;
        j++;
    }
    if(binario[7] == 0){
        return 1; //unidifusión
    }else{
        return 2; //multidifusión
    }
}
